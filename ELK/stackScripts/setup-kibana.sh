#!/bin/bash

set -euo pipefail

es_url=http://elastic:${ELASTIC_PASSWORD}@elasticsearch:9200

# Wait for Elasticsearch to start up before doing anything.
until curl -s $es_url -o /dev/null; do
    sleep 1
done

curl -XPOST $es_url/_security/role/events_admin -H "Content-Type: application/json" -d '{
  "indices" : [
    {
      "names" : [ "events*" ],
      "privileges" : [ "all" ]
    },
    {
      "names" : [ ".kibana*" ],
      "privileges" : [ "manage", "read", "index" ]
    }
  ]
}'

#http://elastic:changeme@elasticsearch:9200
curl -XPOST $es_url/_security/user/mounir -H "Content-Type: application/json" -d '{
  "password" : "testpassword",
  "full_name" : "Mounir Missaoui",
  "email" : "mounir@fivepoints.fr",
  "roles" : [ "events_admin" ]
}' 
# Set the password for the kibana user.
# REF: https://www.elastic.co/guide/en/x-pack/6.0/setting-up-authentication.html#set-built-in-user-passwords
until curl -s -H 'Content-Type:application/json' \
     -XPUT $es_url/_xpack/security/user/kibana/_password \
     -d "{\"password\": \"${ELASTIC_PASSWORD}\"}"
do
    sleep 2
    echo "Retrying..."
done