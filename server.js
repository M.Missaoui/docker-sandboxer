
const jwt = require("fastify-jwt");
const Fastify = require('fastify')
const path = require('path');
const nconf = require('nconf');
const AutoLoad = require('fastify-autoload');
const Sentry = require('@sentry/node');
const uuidv4 = require('uuid/v4');
const swagger = require('./config/swagger');
const io = require('socket.io');
const kue = require("kue");
const fs = require("fs");


Sentry.init({
    dsn: 'https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082'
});

var Docker = require("dockerode");

var datatosend;

const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

const createRequestId = () => uuidv4();

var bodyParser = require('body-parser');

const createServer = (options) => {
    const { logSeverity } = options;
    const fastify = Fastify({
        ignoreTrailingSlash: true,
        logger: {
            genReqId: createRequestId,
            level: logSeverity,
        }
    })

    
    fastify.register(jwt, {
        secret: nconf.get('secrets.jwt'),
    });
    fastify.register(require('fastify-swagger'),swagger.options);



    fastify.register(require('fastify-cors'), {
    })
    fastify.register(require('@guivic/fastify-socket.io'), options, (error) => console.error(error));


    fastify.use(bodyParser.urlencoded({
        extended: false
    }));
    fastify.use(bodyParser.json({
        type: 'application/*+json'
    }));

    fastify.use('io',io);

    fastify.register(AutoLoad,{
        dir: path.join(__dirname, 'api','routes')
    })


    fastify.get('/', function (request, reply) {        
        reply.send({
            hello: 'world'
        })
    })
    const dockeryze = async (Lang, Code, Voucher, identifier) => {
        var dir = "/app/temp";

        if (!fs.existsSync(`${dir}/${identifier}`)) {
            fs.mkdirSync(`${dir}/${identifier}`);
        }
        switch (Lang) {
            case "JS":
                content = `var start = +new Date();
            ${Code}
            var end = +new Date();
            console.log("Process took " + (end-start) + " milliseconds");`;

                fs.writeFileSync(`/app/temp/${identifier}/Code.js`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "nodevm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/sandbox/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            //req.socket.open();
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );

                break;
            case "PHP":
                content = `
            <?php
            $time1 = microtime(true);
            ${Code}
            echo "\nProcess took ". number_format(microtime(true) - $time1, 6). " seconds.";
            ?>`;
                fs.writeFileSync(`/app/temp/${identifier}/Code.php`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "phpvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );
                break;
            case "JAVA":
                content = `public class main{
    public static void main(String []args){
        ${Code}
        }
    }
            
            `

                fs.writeFileSync(`/app/temp/${identifier}/main.java`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "javavm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );



                break;
            case "Python":
                content = `
import os
from datetime import datetime
start_time = datetime.now()
${Code}
time_elapsed = datetime.now() - start_time
print('Process took {}'.format(time_elapsed))`;
                fs.writeFileSync(`/app/temp/${identifier}/Code.py`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "pythonvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            
                            dataObj = {
                                voucher: Voucher,
                                data
                            };

                            fastify.io.emit(`executionDone`, dataObj);
                        });
                    })
                );

                break;
            case "C":
                content = `#include <stdio.h>

int main()
{
    ${Code}

    return 0;
}`
                fs.writeFileSync(`/app/temp/${identifier}/main.c`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "cvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            console.log("container logs", data);
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );

                break;
            case "C++":
                content = `#include <iostream>
                    #include <string>

                    int main()
                    {
                        ${Code}
                    }`;
                fs.writeFileSync(`/app/temp/${identifier}/main.cpp`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "cppvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            
                            console.log("container logs", data);
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );
                break;
            case "C#":
                content = `public class main
                        {
                            public void main()
                            {
                                ${Code}
                            }
                        }`;
                fs.writeFileSync(`/app/temp/${identifier}/main.cs`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "csharpvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {
                            console.log("container logs", data);
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );
                break;
            case "TS":
                content = `${Code}`;
                fs.writeFileSync(`/app/temp/${identifier}/Code.ts`, content, (err) => {
                    if (err) throw err;
                    console.log("File Successully created !");
                });

                docker.run(
                    "tsvm:latest",
                    [],
                    process.stdout, {
                        Volumes: {
                            "/input": {}
                        },
                        name: `Box-${Voucher}-${Lang}-${identifier}`,
                        Labels: {
                            nature: "sandbox",
                        },
                        Hostconfig: {
                            Binds: [`/home/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
                        }
                    },
                    await (async (err, data, container) => {
                        if (err) {
                            return console.error(err);
                        }

                        container.logs({
                            stdout: true,
                            stderr: true
                        }, (err, data) => {

                            console.log("container logs", data);
                            dataObj = {
                                voucher: Voucher,
                                result: data
                            };
                            datatosend = dataObj;
                            console.log("data to emit", dataObj);
                            fastify.io.emit(`executionDone`, dataObj);

                        });
                    })
                );
                break;
        }
    };
    fastify.post('/sandboxes/create', async (req, reply) => {
        var Voucher = req.body.Voucher;
        var Code = req.body.Code;
        var Lang = req.body.Lang;
        var identifier = req.body.timestamp;
        dockeryze(Lang,Code,Voucher,identifier);
        
        reply.send({
            result: datatosend
        })
    })

    //Start the Server
        fastify.listen(3600, '0.0.0.0', function (err, address) {
            if (err) {
                Error = err;
                Sentry.captureMessage(err);
                console.error(err);
                process.exit(1);
            }
            fastify.swagger();
            
            fastify.log.info(`Server Listening on ${address}`)
        })
        
}
// var apm = require('elastic-apm-node').start({
//     // Override service name from package.json
//     // Allowed characters: a-z, A-Z, 0-9, -, _, and space
//     serviceName: '',
  
//     // Use if APM Server requires a token
//     secretToken: '',
  
//     // Set custom APM Server URL (default: http://localhost:8200)
//     serverUrl: 'http://back.sandboxer.fivepoints.fr:8200'
//   })
  


module.exports = {
    createServer, 
}
