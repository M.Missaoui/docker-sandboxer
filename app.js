const nconf = require('nconf');
const server = require('./server');

const {connectMongo} =require('./api/db/db');
const { loadSettings } = require('./config/configurationAdaptor');

const appSettingsPath = process.env.APP_SETTINGS_FILE_PATH;

// Add this to the VERY top of the first file loaded in your app

loadSettings({appSettingsPath})
    .then(() => {
        console.log(nconf.get('db.mongodb.uri'));
        
        const mongoURI = nconf.get('db.mongodb.uri');
        connectMongo(mongoURI);

        const serverOptions = {
            logSeverity: nconf.get('logSeverity'),
        };
        server.createServer(serverOptions);
        // TODO start the Server
        //console.log(server.SocketServer);

    })
    .catch((err)=> {
        console.log(err);
        
    })