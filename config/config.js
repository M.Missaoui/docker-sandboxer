const appName = 'Docker Sandboxer API';
const { APP_SETTINGS_FILE_PATH, port = 3600 } = process.env;


module.exports = {
    appName,
    port,
    appSettingsFilePath: APP_SETTINGS_FILE_PATH,
};
