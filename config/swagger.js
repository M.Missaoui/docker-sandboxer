
exports.options = {
    routePrefix: '/docs',
    exposeRoute: true,
    swagger: {
        info: {
            title: 'Docker Sandboxer Back-End',
            description: 'A client used to create docker sandboxes and manage containers through a fluid and simple UI',
            version: '1.16.3'
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
            
        },
        host: 'https://back.sandboxer.fivepoints.fr',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json']
    }
}