/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./api/db/db.js":
/*!**********************!*\
  !*** ./api/db/db.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\n\nvar connectMongo = function connectMongo(mongoURI) {\n  mongoose.connect(mongoURI, {\n    useNewUrlParser: true\n  });\n  mongoose.connection.on('error', function (err) {\n    console.log(err);\n    process.exit();\n  });\n};\n\nmodule.exports = {\n  connectMongo: connectMongo\n};\n\n//# sourceURL=webpack:///./api/db/db.js?");

/***/ }),

/***/ "./app.js":
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var nconf = __webpack_require__(/*! nconf */ \"nconf\");\n\nvar server = __webpack_require__(/*! ./server */ \"./server.js\");\n\nvar _require = __webpack_require__(/*! ./api/db/db */ \"./api/db/db.js\"),\n    connectMongo = _require.connectMongo;\n\nvar _require2 = __webpack_require__(/*! ./config/configurationAdaptor */ \"./config/configurationAdaptor.js\"),\n    loadSettings = _require2.loadSettings;\n\nvar appSettingsPath = process.env.APP_SETTINGS_FILE_PATH;\nloadSettings({\n  appSettingsPath: appSettingsPath\n}).then(function () {\n  console.log(nconf.get('db.mongodb.uri'));\n  var mongoURI = nconf.get('db.mongodb.uri');\n  connectMongo(mongoURI);\n  var serverOptions = {\n    logSeverity: nconf.get('logSeverity')\n  };\n  server.createServer(serverOptions); // TODO start the Server\n})[\"catch\"](function (err) {\n  console.log(err);\n});\n\n//# sourceURL=webpack:///./app.js?");

/***/ }),

/***/ "./config/configurationAdaptor.js":
/*!****************************************!*\
  !*** ./config/configurationAdaptor.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var nconf = __webpack_require__(/*! nconf */ \"nconf\");\n\nvar _ = __webpack_require__(/*! lodash */ \"lodash\");\n\nvar loadSettings = function loadSettings(_ref) {\n  var appSettingsPath = _ref.appSettingsPath;\n  return new Promise(function (resolve, reject) {\n    try {\n      if (_.isEmpty(appSettingsPath)) {\n        throw new Error('Configuration settings path is required.');\n      }\n\n      nconf.file({\n        file: appSettingsPath,\n        // Setting the separator as dot for nested objects\n        logicalSeparator: '.'\n      });\n      resolve();\n    } catch (err) {\n      reject(err);\n    }\n  });\n};\n\nmodule.exports.loadSettings = loadSettings;\n\n//# sourceURL=webpack:///./config/configurationAdaptor.js?");

/***/ }),

/***/ "./config/swagger.js":
/*!***************************!*\
  !*** ./config/swagger.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("exports.options = {\n  routePrefix: '/docs',\n  exposeRoute: true,\n  swagger: {\n    info: {\n      title: 'Docker Sandboxer Back-End',\n      description: 'A client used to create docker sandboxes and manage containers through a fluid and simple UI',\n      version: '1.16.3'\n    },\n    externalDocs: {\n      url: 'https://swagger.io',\n      description: 'Find more info here'\n    },\n    host: '18.197.194.84:3600',\n    schemes: ['http'],\n    consumes: ['application/json'],\n    produces: ['application/json']\n  }\n};\n\n//# sourceURL=webpack:///./config/swagger.js?");

/***/ }),

/***/ "./server.js":
/*!*******************!*\
  !*** ./server.js ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var jwt = __webpack_require__(/*! fastify-jwt */ \"fastify-jwt\");\n\nvar Fastify = __webpack_require__(/*! fastify */ \"fastify\");\n\nvar path = __webpack_require__(/*! path */ \"path\");\n\nvar nconf = __webpack_require__(/*! nconf */ \"nconf\");\n\nvar AutoLoad = __webpack_require__(/*! fastify-autoload */ \"fastify-autoload\");\n\nvar Sentry = __webpack_require__(/*! @sentry/node */ \"@sentry/node\");\n\nvar uuidv4 = __webpack_require__(/*! uuid/v4 */ \"uuid/v4\");\n\nvar swagger = __webpack_require__(/*! ./config/swagger */ \"./config/swagger.js\");\n\nSentry.init({\n  dsn: 'https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082'\n});\n\nvar createRequestId = function createRequestId() {\n  return uuidv4();\n};\n\nvar bodyParser = __webpack_require__(/*! body-parser */ \"body-parser\");\n\nvar createServer = function createServer(options) {\n  var logSeverity = options.logSeverity;\n  var fastify = Fastify({\n    ignoreTrailingSlash: true,\n    logger: {\n      genReqId: createRequestId,\n      level: logSeverity\n    }\n  });\n  fastify.register(jwt, {\n    secret: nconf.get('secrets.jwt')\n  });\n  fastify.register(__webpack_require__(/*! fastify-swagger */ \"fastify-swagger\"), swagger.options);\n  fastify.register(__webpack_require__(/*! fastify-cors */ \"fastify-cors\"), {// put your options here\n  });\n  fastify.use(bodyParser.urlencoded({\n    extended: false\n  }));\n  fastify.use(bodyParser.json({\n    type: 'application/*+json'\n  }));\n  fastify.register(AutoLoad, {\n    dir: path.join(__dirname, 'api', 'routes')\n  });\n  fastify.get('/', function (request, reply) {\n    reply.send({\n      hello: 'world'\n    });\n  }); //Start the Server\n\n  fastify.listen(3600, '0.0.0.0', function (err, address) {\n    if (err) {\n      Error = err;\n      Sentry.captureMessage(err);\n      console.error(err);\n      process.exit(1);\n    }\n\n    fastify.swagger();\n    fastify.log.info(\"Server Listening on \".concat(address));\n  });\n};\n\nmodule.exports = {\n  createServer: createServer\n};\n\n//# sourceURL=webpack:///./server.js?");

/***/ }),

/***/ 0:
/*!*************************************!*\
  !*** multi babel-polyfill ./app.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! babel-polyfill */\"babel-polyfill\");\nmodule.exports = __webpack_require__(/*! ./app.js */\"./app.js\");\n\n\n//# sourceURL=webpack:///multi_babel-polyfill_./app.js?");

/***/ }),

/***/ "@sentry/node":
/*!*******************************!*\
  !*** external "@sentry/node" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@sentry/node\");\n\n//# sourceURL=webpack:///external_%22@sentry/node%22?");

/***/ }),

/***/ "babel-polyfill":
/*!*********************************!*\
  !*** external "babel-polyfill" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-polyfill\");\n\n//# sourceURL=webpack:///external_%22babel-polyfill%22?");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n\n//# sourceURL=webpack:///external_%22body-parser%22?");

/***/ }),

/***/ "fastify":
/*!**************************!*\
  !*** external "fastify" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fastify\");\n\n//# sourceURL=webpack:///external_%22fastify%22?");

/***/ }),

/***/ "fastify-autoload":
/*!***********************************!*\
  !*** external "fastify-autoload" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fastify-autoload\");\n\n//# sourceURL=webpack:///external_%22fastify-autoload%22?");

/***/ }),

/***/ "fastify-cors":
/*!*******************************!*\
  !*** external "fastify-cors" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fastify-cors\");\n\n//# sourceURL=webpack:///external_%22fastify-cors%22?");

/***/ }),

/***/ "fastify-jwt":
/*!******************************!*\
  !*** external "fastify-jwt" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fastify-jwt\");\n\n//# sourceURL=webpack:///external_%22fastify-jwt%22?");

/***/ }),

/***/ "fastify-swagger":
/*!**********************************!*\
  !*** external "fastify-swagger" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fastify-swagger\");\n\n//# sourceURL=webpack:///external_%22fastify-swagger%22?");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"lodash\");\n\n//# sourceURL=webpack:///external_%22lodash%22?");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"mongoose\");\n\n//# sourceURL=webpack:///external_%22mongoose%22?");

/***/ }),

/***/ "nconf":
/*!************************!*\
  !*** external "nconf" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"nconf\");\n\n//# sourceURL=webpack:///external_%22nconf%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "uuid/v4":
/*!**************************!*\
  !*** external "uuid/v4" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"uuid/v4\");\n\n//# sourceURL=webpack:///external_%22uuid/v4%22?");

/***/ })

/******/ });