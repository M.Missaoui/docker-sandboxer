var dockerode = require('dockerode');

var Docker = new dockerode({ socketPath: '/var/run/docker.sock' });

Docker.createContainer({
    Image: "nodevm:latest",
    Volumes: { "/input": {} },
    HostConfig: {
        Binds: [
            "/home/ubuntu/docker-sandboxer/temp/input:/input:rw"
        ]
    }
}, async (err, container) => {
    console.log("Hi Stream Attachement is done!");
    container.attach(
        { stream: true, stdout: true, stderr: true, tty: true },
        async (err, stream) => {
            console.log("Pipe passed");
            stream.pipe(process.stdout);
            container.start({}, async (err, data) => {
                console.log(data);
            });
        }
    );
}
);