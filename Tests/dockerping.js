var Docker = require("dockerode");
const Sentry = require("@sentry/node");
const os = require('os');
if (os.type()=='Linux') {
    const docker = require('../utils/docker-init');
} else if(os.type()=='Darwin') {
    var docker = new Docker({ socketPath: '/var/run/com.docker.vmnetd.sock' });
}
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
    });

docker.ping((err)=>{
if(err){
    console.error(err);
    Sentry.captureMessage(new Error(JSON.stringify(err)));
}
else {
    console.info("Test passed... Docker running and available");
    Sentry.captureMessage("Test passed... Docker running and available", new Sentry.Severity.Info);
}
});
