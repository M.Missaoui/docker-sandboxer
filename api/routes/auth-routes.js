const {validatePostLogin, validatePostSignup } = require('../Validations/Auth');
const {postLogin, postSignup } = require('../controllers/authController');
const { getUsersCtrl } = require('../controllers/userController');


module.exports= async (fastify) => {
    fastify.post('/auth/login', validatePostLogin, postLogin);
    fastify.post('/auth/signup', validatePostSignup, postSignup);
    fastify.get('/auth/users',getUsersCtrl)
}
