var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
const {
    mapCreateContainer,
    mapInspectContainer,
    mapListContainers,
    mapPauseContainer,
    mapRemoveContainer,
    mapStopContainer,
    mapUnPauseContainer,
    mapRunContainer,
    mapStartContainer
} = require("./../mappers/containers.map");
const {
    createContainer,
    inspectContainer,
    listContainer,
    pauseContainer,
    unPauseContainer,
    stopContainer,
    removeContainer,
    runContainer,
    startContainer,
    pruneContainers,
    getSandboxes,
    getContainerLogs,
    restartContainer,
    //createSandbox
} = require("./../controllers/containerController");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = require('../utils/docker-init');

module.exports = async(fastify) => {
    fastify.get("/containers",listContainer),
    fastify.get("/sandboxes",getSandboxes),
    //fastify.post('/sandboxes/create',createSandbox),
    fastify.post("/containers/create",createContainer),
    fastify.post("/containers/run",runContainer),
    fastify.get("/container/prune",pruneContainers),
    fastify.get("/containers/:containerId", inspectContainer),
    fastify.get("/containers/:containerId/stop",stopContainer),
    fastify.get("/containers/:containerId/unpause",unPauseContainer),
    fastify.get("/containers/:containerId/remove/:RemoveVolumes",removeContainer),
    fastify.get("/containers/:containerId/pause",pauseContainer),
    fastify.get("/containers/:containerId/start",startContainer),
    fastify.get("/containers/:containerId/restart",restartContainer),
    fastify.get("/containers/:containerId/logs",getContainerLogs)

};
