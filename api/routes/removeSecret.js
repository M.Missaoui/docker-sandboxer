var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});


const docker = require('../utils/docker-init');

var RemoveSecret = async fastify => {
    var opts = {
        schema: {
            response: {
                200: {
                    type: "object",
                    properties: {
                        status: {
                            type: "string"
                        }
                    }
                }
            }
        }
    }
    fastify.get("/secrets/:secretId/remove", async (request, reply) => {
        docker.getSecret(request.params.secretId).remove((err, data) => {
            if (err) {
                Sentry.captureException(err);
                console.log(err)
            }
            reply.send({
                Secret: data
            })
        })
    })
}

module.exports = RemoveSecret;