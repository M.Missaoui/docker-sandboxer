var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});

const { getSystemInfo, getVersion, pingDocker, dataUsage, getEvents } = require('../controllers/systemController');
const { SaveDAuth } = require('../controllers/DauthController');

const docker = require('../utils/docker-init');


module.exports = async (fastify) => {
    fastify.get("/system/info", getSystemInfo)
    fastify.get("/system/version", getVersion)
    fastify.get("/system/ping", pingDocker)
    fastify.get("/system/data", dataUsage)
    fastify.post("/system/dauth", SaveDAuth)
    fastify.get("/system/events", getEvents)
}