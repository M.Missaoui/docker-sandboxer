var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});


const docker = require('../utils/docker-init');

var ListSecrets = async fastify => {
        var opts = {
            schema: {

                response: {
                    200: {
                        type: "object",
                        properties: {
                            Secrets: {
                                type: "string"
                            },
                            status: {
                                type: "string"
                            }
                        }
                    }
                }
            }
        }
        fastify.get("/secrets", async (request, reply) => {
            docker.listSecrets({}, (err, secrets) => {
                reply.send({
                    SecretsList: secrets,
                });
            })
        })
    }

module.exports = ListSecrets;