var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var io = require("socket.io-client");
var socket = io.connect("http://213.32.65.59:3000", {
  reconnect: true,
  transports: ["websocket"],
  upgrade: false
});
var Docker = require("dockerode");
Sentry.init({
  dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});

// var docker = new Docker({protocol:'tcp',host: '127.0.0.1',port: 2375});
const docker = require('../utils/docker-init');


docker.ping();

var kue = require("kue"),
  queue = kue.createQueue();

const route = async fastify => {
  var opts = {
    schema: {
      querystring: {
        type: "object",
        properties: {
          Lang: {
            type: "string"
          },
          Code: {
            type: "string"
          },
          Voucher: {
            type: "string"
          }
        }
      },
      response: {
        200: {
          type: "object",
          properties: {
            data: {
              type: "object"
            },
            status: {
              type: "string"
            }
          }
        }
      }
    }
  };
  var datatosend;
  fastify.post("/launch", async (request, reply) => {
    // process.cwd();
    var Dockerjob = queue
      .create("sandbox", {
        Language: request.body.Lang,
        Code: request.body.Code,
        Voucher: request.body.Voucher,
        identifier: request.body.timestamp
      })
      .removeOnComplete(true)
      .attempts(1)
      .save((err) => {
        if (!err) {
          console.log(Dockerjob.id);
        }
      });
    var Voucher = request.body.timestamp;
    var Code = request.body.Code;
    var Lang = request.body.Lang;
    var identifier = request.body.timestamp;
    Dockerjob.on("failed", (errorMessage) => {
      console.log("Job failed");
      let error = JSON.parse(errorMessage);

      console.log(error);
    });

    const dockeryze = await (async (Lang, Code, Voucher, identifier) => {
      var dir = "/app/temp";

      if (!fs.existsSync(`${dir}/${identifier}`)) {
        fs.mkdirSync(`${dir}/${identifier}`);
      }
      switch (Lang) {
        case "JS":
          content = `var start = +new Date();
          ${Code}
          var end = +new Date();
          console.log("Process took " + (end-start) + " milliseconds");`;

          fs.writeFileSync(`/app/temp/${identifier}/Code.js`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "nodevm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };
                datatosend = dataObj
                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);

              });
            })
          );

          break;

        case "PHP":
          // process.chdir(`./temp/${identifier}/`);
          /*child = exec('npm install exec-php --silent',  (err,stdout,stderr)=>{
            console.log('stdout: ' + stdout);
            console.log('stderr: '+ stderr);
            if (err !== null){
              console.log('exec error: '+ err);
              
            }
          });*/
          //process.chdir("/root/docker-sandboxer/");
          content = `
          <?php
          $time1 = microtime(true);
          ${Code}
          echo "\nProcess took ". number_format(microtime(true) - $time1, 6). " seconds.";
          ?>`;
          fs.writeFileSync(`/app/temp/${identifier}/Code.php`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "phpvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                socket.emit(`${Voucher}`, {
                  result: data
                });
              });
            })
          );
          break;

        case "JAVA":
          content = `public class main{
     public static void main(String []args){
         ${Code}
       }
      }
           
           `

          fs.writeFileSync(`/app/temp/${identifier}/main.java`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "javavm:latest",
            [],
            process.stdout, { 
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );



          break;

        case "Python":
          content = `import os
          from datetime import datetime
          start_time = datetime.now()
          ${Code}
          time_elapsed = datetime.now() - start_time
          print('Process took {}'.format(time_elapsed))`;
          fs.writeFileSync(`/app/temp/${identifier}/Code.py`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "pythonvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );

          break;

        case "C":
          content = `#include <stdio.h>

int main()
{
   ${Code}

    return 0;
}`
          fs.writeFileSync(`/app/temp/${identifier}/main.c`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "cvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );

          break;
        case "C++":
          content = `#include <iostream>
                    #include <string>

                    int main()
                    {
                        ${Code}
                    }`;
          fs.writeFileSync(`/app/temp/${identifier}/main.cpp`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "cppvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );
          break;
        case "C#":
          content = `public class main
                      {
                            public void main()
                            {
                                ${Code}
                            }
                      }`;
          fs.writeFileSync(`/app/temp/${identifier}/main.cs`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "csharpvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );
          break;
        case "TS":
          content = `${Code}`;
          fs.writeFileSync(`/app/temp/${identifier}/Code.ts`, content, (err) => {
            if (err) throw err;
            console.log("File Successully created !");
          });

          docker.run(
            "tsvm:latest",
            [],
            process.stdout, {
              Volumes: {
                "/input": {}
              },
              name: identifier
              ,
              Labels: {
                nature: "sandbox",
              }
              ,
              Hostconfig: {
                Binds: [`/root/docker-sandboxer/temp/${identifier}/:/input/:rw`]
              }
            },
            await (async (err, data, container) => {
              if (err) {
                return console.error(err);
              }

              container.logs({
                stdout: true,
                stderr: true
              }, (err, data) => {
                socket.open();
                socket.on("connnect", socket => {
                  console.log("Connect!");
                });
                console.log("container logs", data);
                dataObj = {
                  voucher: Voucher,
                  data
                };

                console.log("socket", socket.connected);
                socket.on("error", error => {
                  console.log("socket err", error);
                });
                console.log("data to emit", dataObj);
                socket.emit(`executionDone`, dataObj);
              });
            })
          );
          break;
      }
    });
    const sandbox = await (async (Dockerjob, done) => {
      dockeryze(
          Dockerjob.data.Language,
          Dockerjob.data.Code,
          Dockerjob.data.Voucher,
          Dockerjob.data.identifier
        )
        .then(success => {

          done();
        })
        .catch(err => {
          if (400 <= err.status <= 499) {
            Dockerjob.attempts(0, () => {
              Sentry.captureMessage(new Error(JSON.stringify(err)));
              return done(new Error(JSON.stringify(err)));
            });
          }
          Sentry.captureMessage(new Error(JSON.stringify(err)));
          return done(new Error(JSON.stringify(err)));
        });
    });
    const processjob = await (async () => {
      queue.process(
        "sandbox",
        10,
        await (async (Dockerjob, done) => {
          sandbox(Dockerjob, done);
        })
      );
    })

    await processjob();

    reply.send({
      result: datatosend
    })

    //  return {
    //   status: "Job assigned to worker ",
    //    data: datatosend
    //  };
  });
};
module.exports = route;