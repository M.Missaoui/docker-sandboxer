var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
const docker = require('../utils/docker-init');
const { listNetworks, inspectNetwork, removeNetwork, pruneNetworks } = require('./../controllers/networksController');
const { mapListNetworks, mapInspectNetwork, mapRemoveNetwork } = require('../mappers/networks.map');
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});



module.exports = async (fastify) => {
    fastify.get("/networks", listNetworks);
    fastify.get("/networks/:netId", inspectNetwork);
    fastify.get("/networks/:netId/remove", removeNetwork);
    fastify.get("/networks/prune", pruneNetworks);
}