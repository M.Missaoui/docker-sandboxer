var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const {listImage,pullPrivate,pullDockerHub,removeImage,inspectImage,pruneImages,pushImage} = require('./../controllers/imageControllers');


const docker = require('../utils/docker-init');


module.exports = async (fastify) =>{
    fastify.get("/images",listImage)
    fastify.get("/images/:imageName",inspectImage)
    fastify.post("/images/pullprivate",pullPrivate)
    fastify.post("/images/pull",pullDockerHub)
    fastify.get("/images/:imageName/remove",removeImage)
    fastify.post("/images/:imageName/push",pushImage)
    fastify.post("/images/prune",pruneImages)
}


