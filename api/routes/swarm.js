var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});


const docker = require('../utils/docker-init');




var  InspectSwarm = async fastify => {
        var opts = {
            schema: {
                response: {
                    200: {
                        type: "object",
                    }
                }
            }
        }
        fastify.get("/Swarm", async (request, reply) => {
            docker.swarmInspect((err, data) => {
                if (err) {
                    console.log(err);
                    Sentry.captureException(err);
                }
                reply.send({
                    Swarm: data,
                })
            })
        })
    }


module.exports = InspectSwarm;