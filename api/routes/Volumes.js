var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});

const { mapListVolumes, mapCreateVolume, mapRemoveVolume, mapInspectVolume } = require('../mappers/volumes.map');
const { ListVolumes, CreateVolume, RemoveVolume, InspectVolume, pruneVolumes } = require('../controllers/volumesController');



const docker = require('../utils/docker-init');

module.exports = async(fastify) => {
    fastify.post("/volumes/create", CreateVolume);
    fastify.get("/volumes/:volumeId/remove", RemoveVolume);
    fastify.get("/volumes/:volumeId", InspectVolume);
    fastify.get("/volumes/", ListVolumes);
    fastify.get("/volumes/prune", pruneVolumes)
}

