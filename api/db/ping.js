const esClient = require('./elasticsearch');

esClient.ping({
    requestTimeout: 3000
}, async (error) => {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
})