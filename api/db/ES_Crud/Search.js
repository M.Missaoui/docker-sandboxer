const esClient = require('./../elasticsearch');
const searchDoc = async (indexName, mappingType, payload) =>{
    return await esClient.search({
        index: indexName,
        type: mappingType,
        body: payload
    });
}

module.exports = searchDoc;


// async function test(){
//     const body = {
//         query: {
//             match: {
//                 "title": "Learn"
//             }
//         }
//     }
//     try {
//         const resp = await searchDoc('blog', ciphertrick, body);
//         console.log(resp);
//     } catch (e) {
//         console.log(e);
//     }
// }

//test()