const esClient = require("./../elasticsearch");

const createIndex = async (indexName) => {
    var body = await esClient.exists({
        index: indexName
    });

    if (body) {
        return "Index Already Exists! Skipping Index creation";
    } else {
        return await esClient.indices.create({
            index: indexName
        });
    }

}

module.exports = createIndex;


/*async function test() {
    try {
        const resp = await createIndex('blog');
        console.log(resp);
    } catch (e) {
        console.log(e);
    }
}
test();*/