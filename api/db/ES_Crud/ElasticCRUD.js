const esClient = require('../elasticsearch');

var ElasticCRUD = {
    AddMapping: async (indexName, mapping) => {
        console.log(mapping);
        return await esClient.indices.putMapping({
            index: indexName,
            type: mappingType,
            body: mapping
        });
    },
    CreateIndex: async (indexName) => {
        var body = await esClient.exists({
            index: indexName
        });

        if (body) {
            return "Index Already Exists! Skipping Index creation";
        } else {
            return await esClient.indices.create({
                index: indexName
            });
        }

    },
    InsertData: async (indexName, _id, data) => {
        return await esClient.index({
            index: indexName,
            type: mappingType,
            id: _id,
            body: data
        });
    },
    Search: async (indexName, payload) => {
        return await esClient.search({
            index: indexName,
            type: mappingType,
            body: payload
        });
    },
    SearchAsYouType: async (indexName, payload) => {
        return await esClient.search({
            index: indexName,
            type: mappingType,
            body: payload
        });
    }

}

module.exports = ElasticCRUD;