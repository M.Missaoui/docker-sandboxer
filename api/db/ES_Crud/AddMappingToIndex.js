const esClient = require("./../elasticsearch");

const addmappingToIndex = async function (indexName, mapping) {
    console.log(mapping);
    return await esClient.indices.putMapping({
        index: indexName,
        type: mappingType,
        body: mapping
    });
}

module.exports = addmappingToIndex;

// async function test() {
//     const mapping = {
//         properties: {
//             title: {
//                 type: "text"
//             },
//             tags: {
//                 type: "keyword"
//             },
//             body: {
//                 type: "text"
//             },
//             timestamp: {
//                 type: "date",
//                 format: "epoch_millis"
//             }
//         }
//     }
//     try {
//         const resp = await addmappingToIndex('blog', 'ciphertrick', mapping);
//         console.log(resp);
//     } catch (e) {
//         console.log(e);
//     }
// }
//test();