const Sentry = require("@sentry/node");
const winston = require('winston');
const Elasticsearch = require('winston-elasticsearch');
const SentryW = require('winston-transport-sentry-node').default;

Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
var Docker = require("dockerode");
const docker = require('../utils/docker-init');

docker.ping();

//indexing container logs in elasticsearch (debate: index all containers or index only sandboxes ? )
docker.listContainers(async (err, containers) => {
    if (err) {
        console.error(err);
        Sentry.captureException(err);
    }
    containers.forEach(async (element) => {
        element.logs({
            stdout: true,
            stderr: true
        }, async (err, data) => {

            const logger = winston.createLogger({
                level: 'verbose',
                format: winston.format.json(),
                defaultMeta: {
                    service: 'containers'
                },
                transports: [
                    new SentryW({

                        sentry: {
                            dsn: 'https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082',
                        },
                        level: 'error'

                    }),
                    new winston.transports.File({
                        filename: 'error.log',
                        level: 'error'
                    }),
                    new winston.transports.File({
                        filename: 'logs.log'
                    }),
                    new Elasticsearch({
                        level: 'info'
                    })
                ]

            });
            if (process.env.NODE_ENV !== 'production') {
                logger.add(new winston.transports.Console({
                    format: winston.format.simple()
                }));
            }

            if (err) {
                console.error(err);
                Sentry.captureException(err);
                logger.error("error flagged at " + Date.now() + " " + err);
            }

            logger.info(data);

        })
    });
});