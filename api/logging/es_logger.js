const Sentry = require("@sentry/node");
var Docker = require("dockerode");
var ElasticService = require("../db/ES_Crud/ElasticCRUD");

Sentry.init({
  dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});

var docker = new Docker({
  socketPath: '/var/run/docker.sock'
});
var data = {
  Id: "",
  Image: "",
  Name: "",
  Command: "",
  logs: "",
  Networks: "",
  createdAt: "",
  Ports: "",
  Mounts: ""
}
docker.ping();

ElasticService.CreateIndex("containers");
const mapping = {
  properties: {
    Id: {
      type: "text"
    },
    Name: {
      type: "keyword"
    },
    Image: {
      type: "text"
    },
    Command: {
      type: "text"
    },
    logs: {
      type: "nested"
    },
    Ports: {
      type: "nested"
    },
    Networks: {
      type: "nested"
    },
    Mounts: {
      type: "nested"
    },
    createdAt: {
      type: "date_nanos",
      format: "epoch_millis"
    },
  }
}
ElasticService.AddMapping("containers", mapping)
//docker.getContainer().id
//indexing container logs in elasticsearch (debate: index all containers or index only sandboxes ? )
docker.listContainers(async (err, containers) => {

  containers.forEach(async (containerInfo) => {

      data.Id = containerInfo.Id;
      data.Image = containerInfo.Image;
      data.Name = containerInfo.Names[0];
      data.Command = containerInfo.Command;
      data.Networks = containerInfo.Networks;
      data.createdAt = containerInfo.Created;
      data.Ports = containerInfo.Ports;
      data.Mounts = containerInfo.Mounts;
    containerInfo.logs({
      stdout: true,
      stderr: true
    }, async (err, data) => {
      data.logs = data;
      ElasticService.InsertData("containers",data.Id,data);
    })
  });
});