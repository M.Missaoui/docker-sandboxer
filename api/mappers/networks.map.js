
const mapListNetworks = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    Networks: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
};

const mapInspectNetwork = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
};

const mapRemoveNetwork = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
};

module.exports = {
    mapInspectNetwork,
    mapListNetworks,
    mapRemoveNetwork,
}