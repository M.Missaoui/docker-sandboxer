const mapInspectImage = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}
const  mapRemoveImage = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
}
const mapListImages = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    Images: {
                        type: ["object"]
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapPullPrivate = {
    schema: {
        querystring: {
            type: "object",
            properties: {
                Image: {
                    type: "string"
                },
                Auth: {
                    type: "object",
                    properties: {
                        Username: {
                            type: "string"
                        },
                        Password: {
                            type: "string"
                        },
                        auth: {
                            type: "string"
                        },
                        Email: {
                            type: "string"
                        },
                        ServerAddress: {
                            type: "string"
                        }
                    }
                }
            }
        }
    }
}

const mapPullDockerHub = {
    schema: {
        querystring: {
            type: "array",
            items: {
                type: "object"
            }

        }
    }
}
const mapPushImage ={
    schema:{
        querystring: {
            type: "object",
            properties: {
                Tag: {
                    type: "string"
                },
                Auth: {
                    type: "object",
                    properties: {
                        Username: {
                            type: "string"
                        },
                        Password: {
                            type: "string"
                        },
                        ServerAddress: {
                            type: "string"
                        }
                    }
                }

            }
        }
    }
}

module.exports = {
    mapInspectImage,
    mapListImages,
    mapPullDockerHub,
    mapPullPrivate,
    mapRemoveImage,
    mapPushImage
}