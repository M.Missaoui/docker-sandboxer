const mapListVolumes = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    Volumes: {
                        type: ["object"]
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapCreateVolume = {
    schema: {
        querystring: {
            type: "object",
            properties: {
                Name: {
                    type: "string"
                },
                Labels: {
                    type: ["object"]
                }
            }
        },
        response: {
            200: {
                type: "object",
                properties: {
                    Volume: {
                        type: "object"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}
const mapRemoveVolume = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
}
const mapInspectVolume = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
}

module.exports= {
    mapCreateVolume,
    mapInspectVolume,
    mapListVolumes,
    mapRemoveVolume
}