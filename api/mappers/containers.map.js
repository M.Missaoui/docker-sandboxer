const mapListContainers = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
}

const mapCreateContainer = {
    schema: {
        querystring: {
            type: "object",
            properties: {
                Image: {
                    type: "string"
                },
                Cmd: {
                    type: ["string"],
                },
                Name: {
                    type: "string"
                },
                        AttachStdin: {
                            type: "boolean"
                        },
                        AttachStdout: {
                            type: "boolean"
                        },
                        AttachStderr: {
                            type: "boolean"
                        },
                    
                StartOnCreate: {
                    type: "boolean"
                },
                Ports: {
                    type: ["object"],
                    properties: {
                        Port: {
                            type: "object",
                            properties: {
                                IP: {
                                    type: "string"
                                },
                                PrivatePort: {
                                    type: "number"
                                },
                                PublicPort: {
                                    type: "number"
                                },
                                Type: {
                                    type: "string"
                                }
                            }
                        }
                    }
                },
                Tty: {
                    type: "boolean"
                },
                Env: {
                    type: ["string"],
                },

            }
        },
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapInspectContainer = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapPauseContainer = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapRemoveContainer = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapUnPauseContainer = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapStopContainer = {
    schema: {
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapRunContainer = {
    schema: {
        querystring: {
            type: "object",
            properties: {
                Image: {
                    type: "string"
                },
                Cmd: {
                    type: ["string", "number"],
                },
                Name: {
                    type: "string"
                },
                Attach: {
                    type: "object",
                    properties: {
                        Stdin: {
                            type: "boolean"
                        },
                        Stdout: {
                            type: "boolean"
                        },
                        Stderr: {
                            type: "boolean"
                        },
                    }
                },
                PortBindings: {
                    type: ["object"],
                    properties: {
                        Port: {
                            type: "object",
                            properties: {
                                PortNumber: {
                                    type: "string"
                                },
                                Protocol: {
                                    type: "string"
                                },
                                HostPort: {
                                    type: ["object"]
                                }
                            }
                        }
                    }
                },
                Tty: {
                    type: "boolean"
                },
                Volumes: {
                    type: ["object"],
                    properties: {
                        Volumes: {
                            type: "object"
                        },
                        Binds: {
                            type: ["string"]
                        }
                    }
                },
                Env: {
                    type: ["string"],
                },

            }
        },
        response: {
            200: {
                type: "object",
                properties: {
                    ContainerId: {
                        type: "string"
                    },
                    status: {
                        type: "string"
                    }
                }
            }
        }
    }
}

const mapStartContainer = {
    schema: {
        response: {
            200: {
                type: "object",
            }
        }
    }
}

module.exports = {
    mapCreateContainer,
    mapInspectContainer,
    mapListContainers,
    mapPauseContainer,
    mapRemoveContainer,
    mapStopContainer,
    mapUnPauseContainer,
    mapRunContainer,
    mapStartContainer,
}