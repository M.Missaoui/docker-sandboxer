const nconf = require('nconf');
const userPasswordRegex = nconf.get('app.userPasswordRegex');

const validatePostLogin = {
    schema:{
        body:{
            type: 'object',
            properties: {
                username: {type: 'string'},
                password: { type: 'string', format: 'regex', userPasswordRegex},
            },
            required: ['username', 'password'],
        }
    }
};

const validatePostSignup = {
    schema: {
        body: {
            type: 'object',
            properties: {
                username: {type: 'string'},
                email: {type: 'string', format: 'email'},
                password: {
                    type: 'string', format: 'regex', pattern: userPasswordRegex, minLength: 6, maxLength: 20,
                },
            },
            required: ['email', 'username', 'password'],
        },
    },
};


module.exports = {
    validatePostLogin,
    validatePostSignup,
};