const mongoose = require('mongoose');
const bcrypt = require("bcrypt-nodejs");


const DAuthSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    serveraddress: {
        type: String,
        required: true
    },
    token:{
        type: String,
        required: false
    },
    base: {
        type: String,
        required: false
    }
});

DAuthSchema.pre('save', function (next) {
    var dauth = this;
    if (!dauth.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return next(err);
        }
        bcrypt.hash(dauth.password, salt, null, (err, hash) => {
            if (err) {
                return next(err);
            }
            dauth.password = hash;
            next();
        })
    })
});

DAuthSchema.methods.comparePassword = function comparePassword(DAuthPassword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(DAuthPassword, this.password, (err, isMatch) => {
            if (err) {
                reject(err)
            };
            resolve(isMatch);
        })
    })
}
var DAuth = mongoose.model('DAuth', DAuthSchema);
module.exports = DAuth;