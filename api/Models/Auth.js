class SignInResponse {
    constructor({ token }) {
        this.token = token;
    }
}

class SignUpResponse {
    constructor({
        token, refreshToken, expiresIn, username, id, message,
    }) {
        this.token = token;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.username = username;
        this.id = id;
        this.message = message;
    }
}

module.exports = {
    SignInResponse,
    SignUpResponse,
};