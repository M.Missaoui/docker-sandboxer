var Docker = require("dockerode");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
const docker = require('../utils/docker-init');
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const listNetworks = async (req, res) => {
    docker.listNetworks({}, (err, networks) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            NetworkList: networks,
        });
    })
}

const inspectNetwork = async (req, res) => {
    docker.getNetwork(req.params.netId).inspect((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Network: data
        })
    })
}

const removeNetwork = async (req, res) => {
    docker.getNetwork(req.params.netId).remove((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Network: data
        })
    })
}
const pruneNetworks = async(req,res) => {
    docker.pruneNetworks({}).then(value =>{
        res.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err)
        console.log(err);
        
    })
}

module.exports = {
    listNetworks,
    inspectNetwork,
    removeNetwork,
    pruneNetworks
}