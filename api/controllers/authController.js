const nconf = require('nconf');

const User = require("../Models/User.model");
const { SignUpResponse , SignInResponse}  = require("./../Models/Auth");
const {
    INVALID_PASSWORD,
    USER_DOESNT_EXIST,
    USER_EXISTS
} = require("../Models/Errors");

const postSignup = async (req, res) => {
    const { username, email, password } = req.body;
    try {
        let existingUser = await User.findOne({ username: req.body.username });
        if (existingUser == null) {
            existingUser = await User.findOne({ email: req.body.email });
        }
        if (existingUser) {
            res.code(409);
            res.send(new Error(USER_EXISTS));
            return;
        }
        const user = new User({
            email,
            username,
            password
        });
        const newUser = await user.save();
        const { id, username: Username, email: Useremail } = newUser;
        const token = await res.jwtSign(
            { id },
            { expiresIn: nconf.get('app.userJwtExpiry') }
        );
        return res.send(new SignUpResponse({ token, id, Username, Useremail }));
    } catch (error) {
        res.send(error);
    }
};

const postLogin = async (req, res) => {
    const { password, username, email } = req.body;
    try {
        const user = await User.findOne({ username: username }).exec();
        if (!user) {
            res.send(new Error(USER_DOESNT_EXIST));
        }
        const isMatch = await user.comparePassword(password);
        if (isMatch) {
            const { id } = user;
            const token = await res.jwtSign({id: id, username: username }, { expiresIn: nconf.get('app.userJwtExpiry') });
            return res.send(new SignInResponse({token}));

        }
        return res.send(new Error(INVALID_PASSWORD));
    } catch (error) {
        throw error;
    }
};

module.exports = {
    postLogin,
    postSignup,
}