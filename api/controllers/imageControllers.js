var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
const Base64 = require('js-base64').Base64;
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

const listImage = async (request, reply) => {
    docker.listImages({
        all: true
    }, (err, images) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        reply.send({
            imagelist: images
        })
    })
}

const pullPrivate = async (request, reply) => {
    var Image = request.body.Image;
    var Auth = request.body.Auth;

    docker.pull(Image, { 'authconfig': Auth }, (err, stream) => {
        docker.modem.followProgress(stream, onFinished, onProgress);
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        function onFinished(err, output) {
            //output is an array with output json parsed objects
            //...
            if (err) {
                console.log(err);
                Sentry.captureException(err);
            }
        }
        function onProgress(event) {
            //...
        }
    })
}

const pullDockerHub = async (request, reply) => {
    docker.pull(request.body.Image, function (err, stream) {
        docker.modem.followProgress(stream, onFinished, onProgress);
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        function onFinished(err, output) {
            //output is an array with output json parsed objects
            //...
            if (err) {
                console.log(err);
                Sentry.captureException(err);
            }
            reply.send({
                Result: output
            })
        }
        function onProgress(event) {
            //...
        }
    })
}

const removeImage = async (request, reply) => {
    docker.getImage(request.params.imageName).remove((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        reply.send({
            Image: data
        })
    })
}

const inspectImage = async (request, reply) => {
    docker.getImage(request.params.imageName).inspect((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        reply.send({
            Image: data
        })
    })

}
const pushImage = async (request, reply) => {
    var auth = {
        "username": request.body.Auth.Username,
        "password": request.body.Auth.Password,
        "serveraddress": request.body.Auth.ServerAddress,
    }
    var auth64 = Base64.encode(JSON.stringify(auth));

    await docker.getImage(request.params.imageName).push({
        authconfig: { base64: auth64 },
        tag: request.body.Tag,
    }, (err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err);

        } else {
            reply.send({
                Info: data
            })
        }

    })
}
var pruneImages = async (request, reply) => {
    docker.pruneImages({all: true}, (err, data) => {
        if (err) {
            Sentry.captureException(err)
            console.log(err);

        } else {
            reply.send({
                Info: data
            })
        }
    })

}

module.exports = {
    inspectImage,
    removeImage,
    listImage,
    pullDockerHub,
    pullPrivate,
    pushImage,
    pruneImages

}
