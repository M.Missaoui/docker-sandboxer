var Docker = require('dockerode');
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

const ListVolumes = async (request, reply) => {
    docker.listVolumes({}, (err, volumes) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        reply.send({
            Volumes: volumes,
            status: 200
        })
    })
}

const CreateVolume = async (request, reply) => {
    var name = request.body.Name;
    var Labels = {} ;
    //request.body.Labels;
    var Driver = "local";

    docker.createVolume({ Name: name, Driver: Driver, Labels: Labels }, async (err, data) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        reply.send({
            volume: data,
            status: 200
        })
    })
}

const RemoveVolume = async (request, reply) => {
    docker.getVolume(request.params.volumeId).remove((err, data) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        reply.send({
            volume: data,
        })
    })
}

const InspectVolume = async (request, reply) => {
    docker.getVolume(request.params.volumeId).inspect((err, data) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        }
        reply.send({
            volume: data,
        })
    })
}
const pruneVolumes = async (request, reply) => {
    docker.pruneVolumes({}).then(value => {
        reply.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err)
        console.log(err);
    })
}

module.exports = {
    InspectVolume,
    RemoveVolume,
    CreateVolume,
    ListVolumes,
    pruneVolumes
}