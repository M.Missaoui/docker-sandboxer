var Docker = require('dockerode');
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

const getSystemInfo = async (req, res) => {
    docker.info().then(value => {
        res.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err);
        console.log(err);

    })
}

const getVersion = async (req, res) => {
    docker.getVersion().then(value => {
        res.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err);
        console.log(err);
    })
}
const pingDocker = async (req, res) => {
    docker.ping().then(value => {
        res.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err);
        console.log(err);

    })
}

const dataUsage = async (req, res) => {
    docker.df().then(value => {
        res.send({
            info: value
        })
    }).catch(err => {
        Sentry.captureException(err);
        console.log(err);
    })
}
const getEvents = async (req, res) => {
    docker.getEvents({}, (err, stream) => {
        docker.modem.followProgress(stream, onFinished, onProgress)
        if (err) {
            Sentry.captureException(err);
            console.log(err);
        }
        function onFinished(err, output) {
            if (err) {
                Sentry.captureException(err);
                console.log(err);
            } else {
                res.send({
                    info: output
                })
            }
        }
        function onProgress(event) {
            console.log(event);

        }
    })
}


module.exports = {
    getSystemInfo,
    getVersion,
    pingDocker,
    dataUsage,
    getEvents
}

