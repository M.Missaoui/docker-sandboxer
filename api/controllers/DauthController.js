const DAuth = require("../Models/DAuth.model");
const Base64 = require('js-base64').Base64;
var Docker = require('dockerode');
const Sentry = require("@sentry/node");
var Docker = require("dockerode");
Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

const SaveDAuth = async (req, res) => {
    const { email, password, username, serveraddress } = req.body;
    var DockerAuth = {
        username: username,

        password: password,

        email: email,

        serveraddress: serveraddress
    }
    docker.checkAuth(DockerAuth).then(async response => {
        var base = Base64.encode(JSON.stringify(DockerAuth));
        var token = response.IdentityToken;
        var newDAuth = new DAuth({
            email,
            username,
            password,
            serveraddress,
            token,
            base
        })
        const savedDauth = await newDAuth.save();
        console.log(savedDauth);
        res.send({
            response: response
        })
    }).catch(err => {
        Sentry.captureException(err);
        console.log(err);

    })


}
module.exports = {
    SaveDAuth
}