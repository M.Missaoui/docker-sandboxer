var bodyParser = require("body-parser");
var currentadmin = require("os").userInfo().username;
const fs = require("fs");
const Sentry = require("@sentry/node");
var io = require("socket.io-client");
;
var socket = io.connect("http://213.32.65.59:3000", {
    reconnect: true,
    transports: ["websocket"],
    upgrade: false
});



var Docker = require("dockerode");
var kue = require("kue"),
    queue = kue.createQueue();
var datatosend;

Sentry.init({
    dsn: "https://ff359c8cc159482692c60a937cdb6373@sentry.io/1459082"
});
const docker = new Docker({
    socketPath: "/var/run/docker.sock"
});

docker.ping();


const DeleteLinkedVolumes = async Id => {
    docker.getContainer(Id).inspect((err, data) => {
        if (err) {
            console.log(err);
            Sentry.captureException(err);
        } else {
            data.Mounts.forEach(Volume => {
                docker.getVolume(Volume.Name).remove({
                        force: true
                    },
                    (err, data) => {
                        if (err) {
                            console.log(err);
                            Sentry.captureException(err);
                        }
                    }
                );
            });
        }
    });
};

const listContainer = async (req, res) => {
    
    docker.listContainers({
        all: true
    }, (err, containers) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err);
            res.send(new Error(err));
        }
        console.log(containers);

        res.send({
            Containers: containers,
            Info: "Request fullfilled!"
        })
    })

}
const getSandboxes = async (req, res) => {
    console.log("Hello Sandboxes \n");

    docker.listContainers({
        
        all:true,
        filters: "{\"label\": [\"nature\"]}"
    }, (err, containers) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err);
            res.send(new Error(err));
        }
        console.log(containers);

        res.send({
            sandboxes: containers,
            Info: "Request fullfilled!"
        })
    })
}


const createContainer = async (req, res) => {
    var Image = req.body.Image;
    var Cmd = req.body.Cmd;
    var Name = req.body.Name;
    var Stdin = req.body.AttachStdin;
    var Stdout = req.body.AttachStdout;
    var Stderr = req.body.AttachStderr;

    var StartOnCreate = req.body.StartContainer;
    var Ports = req.body.Ports;
    var Tty = req.body.Tty;
    var Env = req.body.Env;

    if (StartOnCreate === 'true') {
        docker.pull(Image, {}, function (err, stream) {
            docker.modem.followProgress(stream, onFinished, onProgress);
            if (err) {
                console.log(err);
                Sentry.captureException(err);
            }

            function onFinished(err, output) {
                //output is an array with output json parsed objects
                //...

                if (err) {
                    console.log(err);
                    Sentry.captureException(err);
                }
                docker.createContainer({
                    name: Name,
                    Image: Image,
                    //AttachStdin: Stdin,
                    //AttachStdout: Stdout,
                    //AttachStderr: Stderr,
                    Tty: true,
                    ExposedPorts: req.body.ExposedPorts,
                    //Cmd: Cmd,
                    HostConfig: req.body.HostConfig
                }).then(async (container) => {
                    container.start().then(async (err, data) => {
                        res.send({
                            container: container
                        })
                    });

                });

            }

            function onProgress(event) {
                console.log(event);

            }
        })
    } else {
        docker.pull(Image, {}, function (err, stream) {
            docker.modem.followProgress(stream, onFinished, onProgress);
            if (err) {
                console.log(err);
                Sentry.captureException(err);
            }

            function onFinished(err, output) {
                //output is an array with output json parsed objects
                //...

                if (err) {
                    console.log(err);
                    Sentry.captureException(err);
                }
                docker.createContainer({
                    name: Name,
                    Image: Image,
                    //AttachStdin: Stdin,
                    //AttachStdout: Stdout,
                    //AttachStderr: Stderr,
                    Tty: true,
                    ExposedPorts: req.body.ExposedPorts,
                    //Cmd: Cmd,
                    HostConfig: req.body.HostConfig
                }, (err, container) => {
                    res.send({
                        container: container
                    })
                })

            }

            function onProgress(event) {
                console.log(event);

            }
        })
    }
}

const getContainerLogs = async (req, res) => {
    docker.getContainer(req.params.containerId).logs({
        stderr: true,
        stdout: true
    }, (err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        json = JSON.parse(data)
        res.send({
            logs: json
        })
    })
}

const inspectContainer = async (req, res) => {
    console.log(req.params.containerId);
    docker.getContainer(req.params.containerId).inspect((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    })
}


const pauseContainer = async (req, res) => {
    docker.getContainer(req.params.containerId).pause((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    });
}

const removeContainer = async (req, res) => {
    console.log(req.params.RemoveVolumes);

    if (req.params.RemoveVolumes === 'true') {
        docker.getContainer(req.params.containerId).remove((err, data) => {

            if (err) {
                console.log(err);
                Sentry.captureException(err);

            } else {

            }
        });
        DeleteLinkedVolumes(req.params.containerId);
        res.send({
            VolumesPruned: req.params.RemoveVolumes,
            Container: req.params.containerId
        })
    } else {
        docker.getContainer(req.params.containerId).remove((err, data) => {

            if (err) {
                console.log(err);
                Sentry.captureException(err);

            } else {
                res.send({
                    Container: data
                })
            }
        });
    }
}

const unPauseContainer = async (req, res) => {
    docker.getContainer(req.params.containerId).unpause((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    });
}

const stopContainer = async (req, res) => {
    docker.getContainer(req.params.containerId).stop((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    })
}
const runContainer = async (req, res) => {
    docker.run(req.body.Name, req.body.Cmd, process.stdout, {
        Volumes: {
            // : {}
        },
        Hostconfig: {
            //Binds: [`/ubuntu/docker-sandboxer/temp/${identifier}/:/input/:rw`]
        }
    }, (err, data) => {

    })
}

const restartContainer = async (req, res) => {
    docker.getContainer(req.params.containerId).restart({}, (err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    })
}

//const getContainerStats = async(req, res) => {
//    docker.getContainer(req.params.containerId).stats
//}

const startContainer = async (req, res) => {

    docker.getContainer(req.params.containerId).start((err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err)
        }
        res.send({
            Container: data
        })
    });
}

const pruneContainers = async (req, res) => {
    docker.pruneContainers({
        all: true
    }, (err, data) => {
        if (err) {
            Sentry.captureException(err);
            console.log(err);
        } else {
            res.send({
                Info: data
            })
        }
    });
}

module.exports = {
    createContainer,
    inspectContainer,
    listContainer,
    pauseContainer,
    unPauseContainer,
    stopContainer,
    removeContainer,
    runContainer,
    startContainer,
    pruneContainers,
    getSandboxes,
    getContainerLogs,
    restartContainer,
}