const { NodeVM } = require("vm2");
var execPhp = require('exec-php');

const vm = new NodeVM({
  timeout: 3000,
  sandbox: { execPhp },
  require: {
    external: true,
    import: ["exec-php"],
    //resolve: ["exec-php"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'php']

  }
});

Code = `

execPhp('/input/Code.php', '/usr/bin/php', function (error, php, output) {
if(error !== null){
  console.log(error);
} else {
console.log(output);
}
});`



let functionInVM = vm.run(Code, '/input/Code.php');

