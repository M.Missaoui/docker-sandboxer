const { NodeVM } = require("vm2");

const { join } = require("path");
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: {},
  require: {
    external: ["axios"],
    builtin: ["url", "crypto"],
    root: "./",
  }
});

const code = fs.readFileSync("./input/Code.js");

let functionInVM = vm.run(code);

