const { NodeVM } = require("vm2");
const { c } = require("compile-run");
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { c },
  require: {
    external: true,
    import: ["compile-run"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'java'],
    root: "./"

  }
});


const Code = `
const fun = async ()=>{ 
  return  c.runFile('/input/main.c',{
    compilationPath: '/usr/bin/gcc',
});
};
fun().then(result => {
  if(result.stderr != '')
  console.log(result.stderr);
  else
  console.log(result.stdout);
}).catch(err =>{
  console.log(err);
})

`;



let functionInVM = vm.run(Code, '/input/main.c');

