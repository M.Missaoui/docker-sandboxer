const { NodeVM } = require("vm2");
const cs = require("csharp-runner");
const fs = require("fs");
const RawCode = fs.readFileSync("/input/main.cs");
const Class = `

namespace Foo
{
  ${RawCode}
}`;
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { cs, Class },
  require: {
    external: true,
    import: ["csharp-runner"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'cs'],
    root: "./"

  }
});
const Code = `

cs(Class,'Foo.main','main').then(data=>{
           console.log(data)
        })

`;



let functionInVM = vm.run(Code, '/input/main.cs');

