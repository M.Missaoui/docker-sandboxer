const { NodeVM } = require("vm2");
const { cpp } = require("compile-run");
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { cpp },
  require: {
    external: true,
    import: ["compile-run"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'java'],
    root: "./"

  }
});


const Code = `
const fun = async ()=>{ 
  return  cpp.runFile('/input/main.cpp',{
    compilationPath: '/usr/bin/gcc',
});
};
fun().then(result => {
  if(result.stderr != '')
  console.log(result.stderr);
  else
  console.log(result.stdout);
}).catch(err =>{
  console.log(err);
})

`;



let functionInVM = vm.run(Code, '/input/main.cpp');

