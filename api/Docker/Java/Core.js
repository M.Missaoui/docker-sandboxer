const { NodeVM } = require("vm2");
const { java } = require("compile-run");
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { java },
  require: {
    external: true,
    import: ["compile-run"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'java'],
    root: "./"

  }
});


const Code = `
const fun = async ()=>{ 
  return  java.runFile('/input/main.java',{
    compilationPath: '/usr/bin/javac',
    executionPath: '/usr/bin/java'
});
};
fun().then(result => {
  if(result.stderr != '')
  console.log(result.stderr);
  else
  console.log(result.stdout);
}).catch(err =>{
  console.log(err);
})

`;



let functionInVM = vm.run(Code, '/input/main.java');

