const { NodeVM } = require("vm2");
const {python} = require("compile-run");
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { python },
  require: {
    external: true,
    import: ["compile-run"],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js', 'py'],
    root: "./"

  }
});


const Code = `
//const {python} = require("compile-run");

const fun = async ()=>{ 
  return  python.runFile('/input/Code.py',{
    executionPath: 'python'
})
};
fun().then(result => {
  if(result.stderr != '')
  console.log(result.stderr);
  else
  console.log(result.stdout);
}).catch(err =>{
  console.log(err);
})

`;



let functionInVM = vm.run(Code,'/input/Code.py');

