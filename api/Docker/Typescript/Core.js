const { NodeVM } = require("vm2");
const ts = require('ts-compiler');
const node = require('compile-run');
const fs = require("fs");
const vm = new NodeVM({
  timeout: 3000,
  sandbox: { },
  require: {
    external: true,
    import: [],
    builtin: ["url", "crypto"],
    sourceExtensions: ['js'],
    root: "./"

  }
});


ts.compile(
  ['/input/Code.ts'],
  { skipWrite: false },
  function (err, results) {

    if(err != null)
    console.log(err);

    var Code = results[0];
    let functionInVM = vm.run(Code.text);
  });









