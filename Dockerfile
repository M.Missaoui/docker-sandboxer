#FROM keymetrics/pm2:4-stretch

#LABEL Maintainer="FivePoints(contact@fivepoints.fr)"

#RUN npm cache clean -f

#RUN npm install -g n

#RUN n stable


#COPY . /app

#RUN ls

#WORKDIR /app

#RUN ls

#RUN npm install

#CMD [ "pm2-runtime","start","pm2.json" ]


FROM node:lts
WORKDIR /app
COPY . .
RUN npm install
ENV APP_SETTINGS_FILE_PATH='/app/config/appSettings.json'
EXPOSE 3600
CMD ["node","./app.js"]